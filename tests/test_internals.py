import hashlib

import pytest

from hangul_names import defs
from hangul_names.words import SyllableRuleSet, Syllable, Initial, Vowel, Final, Rule


def test_illegal_chars():
    with pytest.raises(ValueError) as e:
        assert SyllableRuleSet()._lookup_letter(" ")
    assert str(e.value) == "32 out of range (< 4352)"

    with pytest.raises(ValueError) as e:
        assert SyllableRuleSet()._lookup_letter("ᇔ")
    assert str(e.value) == "4564 out of range (> 4546)"

    with pytest.raises(KeyError) as e:
        assert SyllableRuleSet()._lookup_letter("ᅇ")


def test_ruleset():
    class Duped(SyllableRuleSet):
        supported_types = ("initials", "vowels", "initials")

    with pytest.warns(RuntimeWarning):
        duped = Duped()


def test_non_hangul_syllable():
    space = Syllable(" ")
    assert space.is_hangul is False, "Space is not hangul"
    assert space.romanize() == " ", "Space romanizes to space"


def test_syllable_str():
    assert str(Syllable("김")) == "김", "Syllable should str() to init argument"


def test_syllable_ending_with():
    expected = EXPECTED_SYLLABLE_SET
    actual = Syllable.all_ending_with("ᇂ")
    diff = actual.difference(expected)
    assert actual == expected, f"Difference: {diff}"


def test_syllable_equality():
    kim = Syllable("김")
    park = Syllable("박")
    kim2 = Syllable("김")
    assert Syllable("ᇂ") != Initial("ᄒ"), "Syllables and initials are never equal"
    assert kim == kim2, "Two instances of the same syllable should be equal"
    assert kim != park, "Two instances of a different syllable should not be equal"


def test_initial_equality():
    hieut = Initial("ᄒ")
    siot = Initial("ᄉ")
    hieut2 = Initial("ᄒ")
    assert hieut == hieut2, "Two instances of the same initial should be equal"
    assert siot != hieut, "Two instances of a different initial should not be equal"
    assert hieut != Syllable("김")


def test_vowel_equality():
    yeo = Vowel("ᅧ")
    ya = Vowel("ᅣ")
    yeo2 = Vowel("ᅧ")
    assert yeo == yeo2, "Two instances of the same vowel should be equal"
    assert yeo != ya, "Two instances of a different vowel should not be equal"


def test_final_equality():
    digeut = Final("ᆮ")
    mieum = Final("ᆷ")
    digeut2 = Final("ᆮ")
    assert digeut == digeut2, "Two instances of the same final should be equal"
    assert digeut != mieum, "Two instances of a different final should not be equal"


# Note: any changes to these hashes warrants a major version upgrade once API
#       is marked stable.
@pytest.mark.parametrize(
    ("lst", "expect"),
    (
        (
            defs.REVISED_INITIALS,
            "6d279116ce6ab444bfe6eb3a73073af3996330e86b302574e4e45f4f22cbefe4",
        ),
        (
            defs.REVISED_VOWELS,
            "70454dde533001622a10bfb15cb06939a8a485e57424e36844d22e9a569f0df4",
        ),
        (
            defs.REVISED_FINALS,
            "39d168948677211420f08f8b84f9bbe44cf0cbc50c622ce9d43d6b08e41e3d45",
        ),
    ),
)
def test_rr_lists(lst, expect):
    chars = "".join(lst)
    ctx = hashlib.sha256()
    ctx.update(chars.encode("utf-8"))
    assert ctx.hexdigest() == expect


@pytest.mark.parametrize(
    ("Class", "init", "expected"),
    (
        (Initial, "ᄈ", "<Initial: ᄈ () U+0x1108/8>"),
        (Vowel, "ᅫ", "<Vowel: ᅫ () U+0x116b/10>"),
        (Final, "ᇂ", "<Final: ᇂ () U+0x11c2/27>"),
        (
            Syllable,
            "김",
            "<Syllable 44608 (김) -> <Initial: ᄀ (g) U+0x1100/0> <Vowel: ᅵ (i)"
            " U+0x1175/20> <Final: ᆷ (m) U+0x11b7/16>>",
        ),
        (
            Syllable,
            "혜",
            "<Syllable 54812 (혜) -> <Initial: ᄒ (h) U+0x1112/18> <Vowel: ᅨ (ye)"
            " U+0x1168/7> <Final: ᆧ () U+0x11a7/0>>",
        ),
    ),
)
def test_repr(Class, init, expected):
    assert (
        repr(Class(init)) == expected
    ), f"{init} is supposed to represent as {expected}"


def test_rule():
    rule = Rule(initial="ᄀ", vowel="ᅯ", result_initial="k")
    assert rule.match() is False, "Rule.match() without parameters should return false"
    assert (
        rule.match(initial=None, vowel=None, final=None) is False
    ), "Rule.match() with all parameters coded to None should return false"
    assert (
        rule.match(initial=Initial("ᄀ")) is False
    ), "Rule.match() for incomplete matches should return false"
    assert (
        rule.match(vowel=Vowel("ᅯ")) is False
    ), "Rule.match() for incomplete matches should return false"
    assert (
        rule.match(initial=Initial("ᄀ"), vowel=Vowel("ᅯ")) is True
    ), "Rule.match() for complete and full matches should return True"
    assert (
        rule.match(initial=Initial("ᄀ"), vowel=Vowel("ᅯ"), final=Final("ᇂ")) is True
    ), "Rule.match() for complete but partial match should return True"
    rule = Rule(vowel="ᅬ", final=Final.NOTHING, result_vowel="oi")
    assert (
        rule.match(initial=Initial("ᄀ"), vowel=Vowel("ᅬ"), final=Final(Final.NOTHING))
        is True
    ), "Rule.match() for complete but partial match should return True"


EXPECTED_SYLLABLE_SET = {
    Syllable("킇"),
    Syllable("쵛"),
    Syllable("킣"),
    Syllable("촿"),
    Syllable("킿"),
    Syllable("촣"),
    Syllable("탛"),
    Syllable("촇"),
    Syllable("탷"),
    Syllable("뿋"),
    Syllable("쳫"),
    Syllable("턓"),
    Syllable("쳏"),
    Syllable("턯"),
    Syllable("첳"),
    Syllable("텋"),
    Syllable("첗"),
    Syllable("텧"),
    Syllable("챻"),
    Syllable("톃"),
    Syllable("쀃"),
    Syllable("챟"),
    Syllable("톟"),
    Syllable("챃"),
    Syllable("톻"),
    Syllable("찧"),
    Syllable("퇗"),
    Syllable("찋"),
    Syllable("퇳"),
    Syllable("쯯"),
    Syllable("툏"),
    Syllable("쯓"),
    Syllable("툫"),
    Syllable("쮷"),
    Syllable("퉇"),
    Syllable("쮛"),
    Syllable("퉣"),
    Syllable("쭿"),
    Syllable("퉿"),
    Syllable("뿧"),
    Syllable("쭣"),
    Syllable("튛"),
    Syllable("쭇"),
    Syllable("튷"),
    Syllable("쬫"),
    Syllable("틓"),
    Syllable("쬏"),
    Syllable("틯"),
    Syllable("쫳"),
    Syllable("팋"),
    Syllable("쫗"),
    Syllable("팧"),
    Syllable("쪻"),
    Syllable("퍃"),
    Syllable("쪟"),
    Syllable("퍟"),
    Syllable("쪃"),
    Syllable("퍻"),
    Syllable("쩧"),
    Syllable("펗"),
    Syllable("쩋"),
    Syllable("펳"),
    Syllable("쨯"),
    Syllable("폏"),
    Syllable("쨓"),
    Syllable("폫"),
    Syllable("짷"),
    Syllable("퐇"),
    Syllable("짛"),
    Syllable("퐣"),
    Syllable("즿"),
    Syllable("퐿"),
    Syllable("즣"),
    Syllable("푛"),
    Syllable("즇"),
    Syllable("푷"),
    Syllable("쥫"),
    Syllable("풓"),
    Syllable("쥏"),
    Syllable("풯"),
    Syllable("줳"),
    Syllable("퓋"),
    Syllable("줗"),
    Syllable("퓧"),
    Syllable("죻"),
    Syllable("픃"),
    Syllable("죟"),
    Syllable("픟"),
    Syllable("죃"),
    Syllable("픻"),
    Syllable("좧"),
    Syllable("핗"),
    Syllable("좋"),
    Syllable("핳"),
    Syllable("졯"),
    Syllable("햏"),
    Syllable("졓"),
    Syllable("햫"),
    Syllable("젷"),
    Syllable("헇"),
    Syllable("젛"),
    Syllable("헣"),
    Syllable("쟿"),
    Syllable("헿"),
    Syllable("쟣"),
    Syllable("혛"),
    Syllable("쟇"),
    Syllable("혷"),
    Syllable("잫"),
    Syllable("홓"),
    Syllable("잏"),
    Syllable("홯"),
    Syllable("읳"),
    Syllable("횋"),
    Syllable("쵷"),
    Syllable("횧"),
    Syllable("춓"),
    Syllable("훃"),
    Syllable("춯"),
    Syllable("훟"),
    Syllable("췋"),
    Syllable("훻"),
    Syllable("췧"),
    Syllable("휗"),
    Syllable("츃"),
    Syllable("휳"),
    Syllable("츟"),
    Syllable("흏"),
    Syllable("츻"),
    Syllable("흫"),
    Syllable("칗"),
    Syllable("힇"),
    Syllable("칳"),
    Syllable("캏"),
    Syllable("캫"),
    Syllable("컇"),
    Syllable("컣"),
    Syllable("컿"),
    Syllable("켛"),
    Syllable("켷"),
    Syllable("콓"),
    Syllable("콯"),
    Syllable("쾋"),
    Syllable("쾧"),
    Syllable("쿃"),
    Syllable("쿟"),
    Syllable("쿻"),
    Syllable("퀗"),
    Syllable("퀳"),
    Syllable("큏"),
    Syllable("큫"),
    Syllable("뾯"),
    Syllable("뾓"),
    Syllable("뽷"),
    Syllable("뽛"),
    Syllable("뼿"),
    Syllable("뼣"),
    Syllable("뼇"),
    Syllable("뻫"),
    Syllable("뻏"),
    Syllable("뺳"),
    Syllable("뺗"),
    Syllable("빻"),
    Syllable("빟"),
    Syllable("빃"),
    Syllable("븧"),
    Syllable("븋"),
    Syllable("뷯"),
    Syllable("뷓"),
    Syllable("붷"),
    Syllable("붛"),
    Syllable("뵿"),
    Syllable("뵣"),
    Syllable("뵇"),
    Syllable("봫"),
    Syllable("봏"),
    Syllable("볳"),
    Syllable("볗"),
    Syllable("벻"),
    Syllable("벟"),
    Syllable("벃"),
    Syllable("뱧"),
    Syllable("뱋"),
    Syllable("밯"),
    Syllable("밓"),
    Syllable("믷"),
    Syllable("믛"),
    Syllable("뮿"),
    Syllable("뮣"),
    Syllable("뮇"),
    Syllable("뭫"),
    Syllable("뭏"),
    Syllable("묳"),
    Syllable("묗"),
    Syllable("뫻"),
    Syllable("뫟"),
    Syllable("뫃"),
    Syllable("몧"),
    Syllable("몋"),
    Syllable("멯"),
    Syllable("멓"),
    Syllable("먷"),
    Syllable("먛"),
    Syllable("맿"),
    Syllable("맣"),
    Syllable("맇"),
    Syllable("릫"),
    Syllable("릏"),
    Syllable("륳"),
    Syllable("륗"),
    Syllable("뤻"),
    Syllable("뤟"),
    Syllable("뤃"),
    Syllable("룧"),
    Syllable("뢯"),
    Syllable("뢓"),
    Syllable("룋"),
    Syllable("롛"),
    Syllable("렿"),
    Syllable("롷"),
    Syllable("렣"),
    Syllable("렇"),
    Syllable("럫"),
    Syllable("럏"),
    Syllable("랳"),
    Syllable("랗"),
    Syllable("띻"),
    Syllable("띟"),
    Syllable("띃"),
    Syllable("뜧"),
    Syllable("뜋"),
    Syllable("윻"),
    Syllable("뙇"),
    Syllable("윟"),
    Syllable("윃"),
    Syllable("웧"),
    Syllable("똫"),
    Syllable("웋"),
    Syllable("욯"),
    Syllable("욓"),
    Syllable("왷"),
    Syllable("왛"),
    Syllable("똏"),
    Syllable("옿"),
    Syllable("뗳"),
    Syllable("뗗"),
    Syllable("옣"),
    Syllable("옇"),
    Syllable("떻"),
    Syllable("엫"),
    Syllable("떟"),
    Syllable("떃"),
    Syllable("엏"),
    Syllable("땧"),
    Syllable("얳"),
    Syllable("땋"),
    Syllable("얗"),
    Syllable("뛯"),
    Syllable("앻"),
    Syllable("앟"),
    Syllable("딓"),
    Syllable("앃"),
    Syllable("뛓"),
    Syllable("씧"),
    Syllable("듛"),
    Syllable("뚷"),
    Syllable("씋"),
    Syllable("뚛"),
    Syllable("쓯"),
    Syllable("쓓"),
    Syllable("쒷"),
    Syllable("뒇"),
    Syllable("쒛"),
    Syllable("쑿"),
    Syllable("뙿"),
    Syllable("쑣"),
    Syllable("쑇"),
    Syllable("쐫"),
    Syllable("둏"),
    Syllable("쐏"),
    Syllable("쏳"),
    Syllable("쏗"),
    Syllable("쎻"),
    Syllable("뙣"),
    Syllable("쎟"),
    Syllable("됗"),
    Syllable("쎃"),
    Syllable("돻"),
    Syllable("돟"),
    Syllable("썧"),
    Syllable("썋"),
    Syllable("쌯"),
    Syllable("쌓"),
    Syllable("싷"),
    Syllable("싛"),
    Syllable("돃"),
    Syllable("슿"),
    Syllable("슣"),
    Syllable("슇"),
    Syllable("뎧"),
    Syllable("쉫"),
    Syllable("쉏"),
    Syllable("뎋"),
    Syllable("덯"),
    Syllable("숳"),
    Syllable("딯"),
    Syllable("숗"),
    Syllable("댷"),
    Syllable("쇻"),
    Syllable("듷"),
    Syllable("댛"),
    Syllable("쇟"),
    Syllable("뒿"),
    Syllable("쇃"),
    Syllable("뒣"),
    Syllable("솧"),
    Syllable("덓"),
    Syllable("솋"),
    Syllable("셯"),
    Syllable("셓"),
    Syllable("섷"),
    Syllable("둫"),
    Syllable("닣"),
    Syllable("섛"),
    Syllable("됳"),
    Syllable("샿"),
    Syllable("닿"),
    Syllable("샣"),
    Syllable("샇"),
    Syllable("삫"),
    Syllable("삏"),
    Syllable("쁳"),
    Syllable("쁗"),
    Syllable("쀻"),
    Syllable("읗"),
    Syllable("쀟"),
    Syllable("굏"),
    Syllable("괳"),
    Syllable("괗"),
    Syllable("곻"),
    Syllable("갛"),
    Syllable("곟"),
    Syllable("곃"),
    Syllable("겧"),
    Syllable("겋"),
    Syllable("걯"),
    Syllable("갷"),
    Syllable("걓"),
    Syllable("눻"),
    Syllable("눟"),
    Syllable("눃"),
    Syllable("뇧"),
    Syllable("뇋"),
    Syllable("놯"),
    Syllable("놓"),
    Syllable("녷"),
    Syllable("녛"),
    Syllable("닇"),
    Syllable("넿"),
    Syllable("늫"),
    Syllable("넣"),
    Syllable("늏"),
    Syllable("뉳"),
    Syllable("넇"),
    Syllable("뉗"),
    Syllable("냫"),
    Syllable("낳"),
    Syllable("냏"),
    Syllable("낗"),
    Syllable("끻"),
    Syllable("끟"),
    Syllable("끃"),
    Syllable("뀧"),
    Syllable("뀋"),
    Syllable("꿯"),
    Syllable("꿓"),
    Syllable("꽇"),
    Syllable("꾛"),
    Syllable("꾷"),
    Syllable("꽿"),
    Syllable("꼫"),
    Syllable("꽣"),
    Syllable("껗"),
    Syllable("깋"),
    Syllable("꼏"),
    Syllable("꺟"),
    Syllable("궇"),
    Syllable("귷"),
    Syllable("귛"),
    Syllable("궿"),
    Syllable("깧"),
    Syllable("궣"),
    Syllable("긓"),
    Syllable("굫"),
    Syllable("껳"),
    Syllable("긯"),
    Syllable("꺃"),
    Syllable("꺻"),
}
